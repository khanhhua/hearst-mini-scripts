'''
Created on May 22, 2013

@author: khanhhua
'''
class Article:
    def __init__(self, *args, **kwargs):        
        self.id = kwargs['id'] if 'id' in kwargs else None
        self.title = kwargs['title'] if 'title' in kwargs else None
        self.url_name = kwargs['url_name'] if 'url_name' in kwargs else None
        self.url = kwargs['url'] if 'url' in kwargs else None