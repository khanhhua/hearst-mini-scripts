'''
Created on May 7, 2013

@author: khanhhua
'''
from core import validator, configurations
from arsenal import config

@validator('ams', 'AMS not contained')
def validate(content): 
    ams_position = configurations['ams_position']
    if (ams_position in content):
        return True
    else:
        return False