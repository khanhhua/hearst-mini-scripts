import sys
from core import configurations

def bg_worker(func): pass

def config(**kwargs):
    def wrapped(target):
        target.configs = kwargs 
        def configured_target():
            required_configs = set(kwargs['required']) if 'required' in kwargs else []
            optional_configs = set(kwargs['optional']) if 'optional' in kwargs else []
            # Automatical check for all required arguments to run a configurable tool
            existing_configs = set(configurations.keys())
            
            if len(required_configs) > 0:                
                if not required_configs.issubset(existing_configs):
                    print 'Required arguments not found:'
                    for missing_required in required_configs - (existing_configs & required_configs):
                        print '- %s' % missing_required
                    return
            # Set up defaults for optional configurations    
            for optional in optional_configs:
                if not optional in existing_configs: 
                    configurations[optional] = None
            target()
            
        return configured_target
    return wrapped    

def task_log(**kwargs):
    def wrapped(target):
        target.task = kwargs['task'] if 'task' in kwargs else None
        target.format = kwargs['format'] if 'format' in kwargs else 'Performing {task_name}'
        target.log = kwargs['log'] if 'log' in kwargs else sys.stdout
        def logged_target(*args):
            if target.task == None:
                target.task = target.__name__
            print target.format.format(*args)
            target(*args)
    
        return logged_target    
    return wrapped

def confirm(message = 'Are you sure? (y/n)'):
    def wrapped(target):
        target.message = message
        def confirmed_target(*args):
            reply = raw_input(target.message)
            reply = reply.lower().strip()
            while not reply in ['y','n']:
                print 'Please answer y or n'
                reply = raw_input(target.message + ' ')
                reply = reply.lower().strip()
                
            if reply == 'y':    
                target(*args)
            else:
                print 'Skipped'
        return confirmed_target
    return wrapped

def http_auth(home_url = None, username = None, password = None, pass_required = True):
    '''
    Decorate a method which requires authentication
    Authentication data can be set at one of the three stages:
    + Code
    + Configuration
    + User prompt 
    '''
    import urllib2
    
    def wrapped(target):
        target.home_url = home_url if home_url != None else configurations.get('home_url', None)
        target.username = username if username != None else configurations.get('username', None)
        target.password = password if password != None else configurations.get('password', None)
        target.pass_required = pass_required
        
        def authorized_target(*args, **kwargs):
            if target.pass_required == True:
                if target.home_url == None:
                    target.home_url = raw_input('Home URL: ')
                if target.username == None:
                    target.home_url = raw_input('URL name: ')
                if target.password == None:
                    from getpass import getpass
                    target.password = getpass('Enter your password: ')
                
            password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()            
            password_mgr.add_password(None, target.home_url, target.username, target.password)
            auth_handler = urllib2.HTTPBasicAuthHandler(password_mgr)
            opener = urllib2.build_opener(auth_handler)
            urllib2.install_opener(opener)
            
            target(*args, **kwargs)
        
        return authorized_target
    return wrapped

def resumable(task_name = None):
    import core
    from core.resume import TaskStore
    
    if core.taskstore == None:
        core.taskstore = TaskStore()
    def wrapped(target):
        if task_name == None:
            target.task_name = target.__name__
        else:
            target.task_name = task_name
        def wrapped_target(*args, **kwargs):
            core.taskstore.record(target.task_name)
            # Carry out the normal flow
            result = target(*args, **kwargs)
            # Cleanup after the task is done
            core.taskstore.commit(target.task_name)
            return result;
        return wrapped_target
    return wrapped

def task_unit(task_name):
    '''
    A task unit maintains reference to a non-divisible (atomic) unit of work (thus the name) 
    '''
    import core
    from core.resume import TaskStore
    
    if core.taskstore == None:
        core.taskstore = TaskStore()
    
    def wrapped(target):
        target.task_name = task_name
        def interruptible_target(*args, **kwargs):
            stat = core.taskstore.checkstat(target.task_name, *args, **kwargs)
            if stat == 2:
                print 'Task skipped. Fast forward to checkpoint'
                return
            elif stat == 1:
                print 'Checkpoint found. Resuming...'
            
            try:
                # The business logics
                result = target(*args, **kwargs)
                core.taskstore.commit(target.task_name)
                return result
            except Exception as x:
                core.taskstore.checkpoint(target.task_name, args, kwargs)
                raise x
                
        return interruptible_target
    return wrapped