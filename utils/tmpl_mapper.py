'''
Created on May 7, 2013

@author: khanhhua
'''
from arsenal import config
from core import configurations
from core.tmpl import Template, Site, CrossSite
import os

entry_point = None
base_dir = None
xsite_dir = None

@config(required=['site','entry-point','base-dir'], optional=['direction','view','xsite-dir'])
def run():
    global base_dir, entry_point, xsite_dir
    
    base_dir = configurations['base-dir']
    if configurations['site'] == 'crosssite':
        site = CrossSite(configurations['site'], base_dir)
    else:
        site = Site(configurations['site'], base_dir)
    entry_point = configurations['entry-point']
    xsite_dir = configurations['xsite-dir'] if configurations['xsite-dir'] != None else 'crosssite' 
    
    tmpl_path = '%s/%s/%s' % (base_dir, site.dir, entry_point)
    if not os.path.exists(base_dir):
        print '%s does not exist' % base_dir
        return
    if not os.path.exists(tmpl_path):
        print '%s does not exist' % entry_point
        return
    
    entry_tmpl = make_tmpl(site, entry_point)
    
    direction = configurations['direction'] if 'direction' in configurations else 'FORWARD'
    direction = 'FORWARD' if direction == None else direction
    view = configurations['view']  
    
    if direction == 'FORWARD':
        map_forward(entry_tmpl)
        if view == None or view == 'tree':
            print entry_tmpl.tree_view()
        elif view == 'expanded':
            print entry_tmpl.expanded_view()
    else:
        pyramid = map_reverse(entry_tmpl)
        show_inverse_tree(entry_tmpl, pyramid)
#         for level in pyramid:
#             for tmpl in level:
#                 print tmpl._name
#             print '====='

def make_tmpl(site, tmpl_filename):
    """
    @param site: Site object
    @type site: core.tmpl.Site 
    """    
    tmplObj = site.tmpl(tmpl_filename)
    return tmplObj

def show_inverse_tree(entry, pyramid, level = 0):
    print '%s\t%s' % (('    ' * level + entry._name).ljust(60, '.'), entry.shortpath())
    if entry._name in pyramid:
        for item in pyramid[entry._name]:
            show_inverse_tree(item, pyramid, level + 1)

def map_forward(tmpl):
    """
    @param tmpl: Template
    @type tmpl: core.tmpl.Template
    """
    crosssite = Site('crosssite', base_dir)
    queue = [tmpl]
    # visited = set()
    while len(queue) > 0:
        entry_tmpl = queue.pop()
        for included in entry_tmpl.deferred_includes:
            if included[0:2] == 'xs' or included[0:5] == 'html5':
                included_tmpl = crosssite.tmpl(included)
            else:
                included_tmpl = tmpl._site.tmpl(included, entry_tmpl)
            
            if included_tmpl != None: # and not tmpl_path in visited:
                entry_tmpl.include(included_tmpl)
                queue.append(included_tmpl)
                # visited.add(tmpl_path)
                
def map_reverse(tmpl, pyramid = {}, all_files = None, site = None):
    """
    @param tmpl: Template
    @type tmpl: core.tmpl.Template
    @param site: Site
    @type site: core.tmpl.Site 
    """
    should_return = False
    
    if all_files == None: 
        all_files = tmpl._site.list_files()
    if site == None:
        site = tmpl._site
        should_return = True
    
    site_base_length = len(site.path())
    
    referrers = []
    for filename in all_files:
        referrer = site.tmpl(filename[site_base_length+1:])
        if referrer == None:
            print filename
            
        elif referrer.hasIncluded(tmpl):             
            referrers.append(referrer)
    
    if len(referrers) > 0: 
        pyramid[tmpl._name] = tuple(referrers)
        for referrer in referrers:            
            map_reverse(referrer, pyramid, all_files, site)
            
    if should_return:
        return pyramid

def report(): pass