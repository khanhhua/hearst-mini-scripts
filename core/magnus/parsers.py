'''
Created on May 22, 2013

@author: khanhhua
'''
from HTMLParser import HTMLParser
from core.magnus import Article
import re

class MagnusArticleListParser():
    def __init__(self):
        self.reset()
    
    def reset(self):
        self.articles = []
        
    def parse(self, html):
        rgx_row = re.compile('\<td\sclass="article_title"\>.+?\</td\>', re.DOTALL)
        rgx_title = re.compile('\<a.+?\>(?P<title>.+?)\<br\s/\>', re.DOTALL)
        rgx_url_name = re.compile('\<span\sclass="url_name"\>\<em\>URL:\</em\>(?P<url_name>.+?)\</span\>')
        rgx_id = re.compile('\[ID: (?P<id>\d+)\]')
        
        rows = rgx_row.findall(html)
        for row in rows:
            match = rgx_title.search(row)
            title = match.group('title') if match != None else '#NOT FOUND#'                
             
            match = rgx_url_name.search(row)
            url_name = match.group('url_name') if match != None else '#NOT FOUND#'
            
            match = rgx_id.search(row)            
            id = int(match.group('id')) if match != None else 0
            
            self.articles.append(Article(id=id, title=title.strip(), url_name=url_name.strip()))