'''
Created on Aug 2, 2013

@author: Khanhhua
'''
import logging
import time
logger = logging.getLogger('hms')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.addHandler(logging.FileHandler('log.data'))

result_filename = 'result-%d.data' % time.time()
result_logger = logging.getLogger('hms.result')
result_logger.setLevel(logging.INFO)
result_logger.addHandler(logging.FileHandler(result_filename,'w'))

def log(msg, cat=None, *args):
    if cat == None:
        cat = 'info'
    
    if len(args) == 0:
        _msg = msg
    else:
        _msg = msg % args
    
    if cat=='result':
        result_logger.info(_msg)
    else:
        logger.info(_msg)
    
def info(msg, *args):
    log(msg, 'info', *args)
        
def result(msg, *args):
    log(msg, 'result', *args)
    
if __name__ == '__main__':
    log('Test')
    log('Test error','error')
    log('Test info %s','info','param1')
    log('Test info %s %s','info','param1','param2')
    
    result('Test result')
    result('Test result %s','param1')
    result('Test result %s %s','param1','param2')