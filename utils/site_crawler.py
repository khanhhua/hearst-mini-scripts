'''
Created on May 7, 2013

@author: khanhhua
'''
import urllib2, re
from core import validators, configurations, filters, pickers
from arsenal import config, task_log
from cookielib import CookieJar

rgx_link = re.compile(r'<a.*?href="(.*?)"', re.IGNORECASE)

home_url = None
all_links = []
picked_links = set()
visited_links = set()
max_links = 500

@config(required=['url'], optional=['recursive', 'limit', 'depth', 'user', 'pass'])
def run():
    global all_links, picked_links, home_url, max_links
    
    url = configurations['url']
    max_links = int(configurations['limit']) if configurations['limit'] != None else max_links  
    
    config_keys = set(configurations.keys())
    match = re.search('^http://[\w\.\-_]+(:\d+)?', url)
    if match != None:
        home_url = match.group(0) 
        if set(['user', 'pass']).issubset(config_keys):
            init_urllib2(home_url, configurations['user'], configurations['pass'])
    crawl(url)
    
def report():
    print 'Links processed %d' % len(all_links)
    for link in visited_links: 
        print link
    print ''
    print 'Links picked %d' % len(picked_links)
    for link in picked_links: 
        print link

@task_log(task='crawl', format='Accessing {}')
def crawl(url):
    global all_links, visited_links, max_links
    queue = [url]
        
    while len(all_links) < max_links and len(queue) != 0:
        url = queue.pop()
        
        content = read_URL(url)
        if content == None: continue
        
        links_onpage = parseLinks(content)
        links_onpage = set(links_onpage) - visited_links
        all_links.extend(links_onpage)
        
        for link in links_onpage:
            if should_crawl(link):
                queue.append(link)
            if should_pick(link):
                picked_links.add(link)
    
def read_URL(url):
    global visited_links
    try:
        request = urllib2.Request(url)
        request.add_header('User-agent', 'Mozilla/5.0 (Linux i686)')
        content = urllib2.urlopen(request).read()
        
        return content
    except:
        return None
    finally:
        # Mark the URL as visited anyways
        print 'visited %s' % url
        visited_links.add(url)
    
def should_crawl(url):
    for sifter in filters:
        if not sifter(url):
            return False
    return True

def should_pick(url):
    for picker in pickers:
        if not picker(url):
            return False
    return True    

def init_urllib2(home_url, user, pwd):
    mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
    mgr.add_password(None,home_url,user,pwd)
    cj = CookieJar()
    urllib2.install_opener(urllib2.build_opener(urllib2.HTTPCookieProcessor(cj), urllib2.HTTPBasicAuthHandler(mgr)))  
    
def parseLinks(line, direction = 'inbound'):
    global home_url
    """
    Parse possible links present in the line given
    """
    links = rgx_link.findall(line)
    #   Verbose output:
    #    for link in links:
    #        print('Found: %s' % link)
    # Check whether or not new_links are acceptable candidates
    if direction == 'inbound':
        # Accumulate
        accepted_links = [link for link in links if link.startswith(home_url) or link.startswith('/')]
        # Make absolute URLs
        for i in range(0, len(accepted_links)):
            if accepted_links[i].startswith('/'):
                accepted_links[i] = home_url + accepted_links[i]
        
        return accepted_links
    else:
        return links