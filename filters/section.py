'''
Created on May 14, 2013

@author: khanhhua
'''
from core import sift, pick

@sift('by-section-name')
@pick('by-section-name')
def section(url):
    print 'Allows only SECTION life'
    return '/life' in url

@sift('sections-only')
@pick('sections-only')
def sections_only(url):