'''
Created on May 7, 2013

@author: khanhhua
'''
from core import validator

@validator('well-formed', 'HMTL not well-formed')
def well_formed(content): 
    print 'Well-Formness validation run complete'
    return True

@validator('html4', 'HMTL is not HTML4-compliant')
def html4(content):
    print 'HTML4 compliance validation run complete'
    return True

@validator('html5', 'HMTL is not HTML5-compliant')
def html5(content):
    print 'HTML5 compliance validation run complete'
    return True