configurations = {'interactive':False}
validators = []
filters = []
pickers = []
taskstore = None

def validator(name, error_msg, group = None, order = None):
    '''
    Decorator to mark a function as a validator method
    '''
    def wrapped(func):
        global validators
        
        vObj = ValidatorDescriptor(func)
        vObj.name = name
        vObj.order = order
        vObj.error_msg = error_msg
        
        validators.append(vObj)
    return wrapped

def sift(name, group = None, order = None):
    '''
    Decorator to mark a function as a sift method
    '''
    def wrapped(func):
        global filters
        filters.append(func)
        
        return func
    return wrapped

def pick(name, group = None, order = None):
    '''
    Decorator to mark a function as a sift method
    '''
    def wrapped(func):
        global filters
        pickers.append(func)
        
        return func
    return wrapped
        
class ValidatorDescriptor(object):
    '''
    classdocs
    '''
    name = None
    order = None
    error_msg = None
    _validate_func = None

    def __init__(self, validate_func):
        '''
        Constructor
        '''
        self._validate_func = validate_func
    
    def set_options(self, options):
        pass
    
    def run(self, content):
        return self._validate_func(content)
                
class ValidationContainer(object):
    '''
    classdocs
    '''


    def __init__(self, validators):
        '''
        Constructor
        '''  