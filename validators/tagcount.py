'''
Created on May 7, 2013

@author: khanhhua
'''
from core import validator, log
import re

@validator('tag-count','More than 1 h1 or h2 were found')
def validate(content): 
    exp_h1 = re.compile('<h1.*?>.*?</h1>', re.DOTALL | re.IGNORECASE)
    exp_h2 = re.compile('<h2.*?>.*?</h2>', re.DOTALL | re.IGNORECASE)
    
    h1_count = len(re.findall(exp_h1, content))
    h2_count = len(re.findall(exp_h2, content))
    
    if h1_count == 1 and h2_count <= 1:
        return True
    else:
        if h1_count != 1:
            log.info('H1 count: %s',h1_count)
        if h2_count > 1:
            log.info('H2 count: %s',h2_count)
        return False