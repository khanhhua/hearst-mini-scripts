'''
Created on May 21, 2013

@author: khanhhua
'''
from arsenal import config, confirm
from core import configurations
import os, re

meebo_targets = [re.compile('^.+?BEGIN MEEBO SCRIPT.+?$'),
                 re.compile('^.+?xs_sites_scripts_meebo_top.tmpl.+?$'),
                 re.compile('^.+?END MEEBO SCRIPT.+?$'),
                 re.compile('^.+?xs_sites_scripts_meebo_bottom.tmpl.+?$')]

files_requested = []
files_updated = []

@config(required=['list'])
def run():
    if 'list' in configurations:
        list_file = configurations['list']
        if not os.path.exists(list_file):
            print 'List file does not exist or is not accessible'
            return
        with open(list_file) as f:
            for line in f:
                # each line is a URL
                line = line.strip()
                files_requested.append(line)
                print 'Accessing %s' % line
                remove(line)
                
def remove(filename):
    meebo_found = []
    
    lines = []
    try:
        with open(filename, 'r') as f:
            line_no = 0
            for line in f:
                line_no += 1
                strip = line.strip()
                found = False
                for rgx in meebo_targets:
                    if not rgx.match(strip) == None: 
                        found = True
                        break
                if found:
                    print ' %d: %s' % (line_no, strip)
                    meebo_found.append(line)
                else:
                    lines.append(line)
        if len(meebo_found) != 4:
            print 'Notice: Not enough meebo script in %s' % filename
            for meebo in meebo_found:
                print '--> %s ' % meebo
        if len(lines) > 0:
            write(filename, lines)
    except IOError:
        print 'Could not open %s' % filename

@confirm()        
def write(filename, lines):
    try:
        f = open(filename, 'wb')
        f.writelines(lines)
        f.close()
        print 'Info: DONE'
        files_updated.append(filename)
    except:
        print 'Could not write %s' % filename
        
def report():
    print 'Files requested: %d' % len(files_requested)
    print 'Files updated: %d' % len(files_updated)
    
    files_not_updated = set(files_requested) - set(files_updated)
    if len(files_not_updated) == 0:
        print 'All requested files was updated'
    else:
        print '\nFiles NOT updated'
        for filename in files_not_updated:
            print filename