'''
Created on May 22, 2013

@author: khanhhua
'''
import urllib2
from arsenal import config, task_log
from core import configurations
import httplib
from core.magnus.parsers import MagnusArticleListParser

magnus_prefix = None
articles = []

@config(required=['user','pass','template'], optional=['magnus','limit'])
def run():
    global magnus_prefix
    
    magnus = configurations['magnus'] if not configurations['magnus'] == None else 'live'
    limit = configurations['limit'] if not configurations['limit'] == None else 25
    
    if magnus == 'live':
        magnus_prefix = 'https://admin.hearstmags.com'
    elif magnus == 'beta':
        magnus_prefix = 'https://betapreview.admin.hearstmags.com'
    elif magnus == 'alpha':
        magnus_prefix = 'https://alphapreview.admin.hearstmags.com'
    
    login(magnus_prefix, configurations['user'], configurations['pass'])
    
    template_id = configurations['template'].split(',')
    find_urls_using_template(template_id, limit)

def login(home_url, username, passwd):
    password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None,home_url, username, passwd)
    auth_handler = urllib2.HTTPBasicAuthHandler(password_mgr)
    opener = urllib2.build_opener(auth_handler)
    urllib2.install_opener(opener)

@task_log(name='find-url', format='Accessing {}')
def find_urls_using_template(template_id, limit = 25):
    global magnus_prefix, articles
    
    if type(template_id) == int:
        url_template_search = '%s/admin/cm/articles/processSearch?limit=%s&article_template_id=%d' % (magnus_prefix, limit, template_id)
        try:
            response = urllib2.urlopen(url_template_search)
            html = response.read()
            response.close()
            
            parser = MagnusArticleListParser()
            parser.parse(html)
            if len(parser.articles) != 0:
                articles.append((template_id, parser.articles))
            
        except urllib2.HTTPError, e:
            pass
            # checksLogger.error('HTTPError = ' + str(e.code))
        except urllib2.URLError, e:
            pass
            # checksLogger.error('URLError = ' + str(e.reason))
        except httplib.HTTPException, e:
            pass
            # checksLogger.error('HTTPException')
        #except Exception, e:    
        #    pass
    elif type(template_id) == list:
        for template_id_as_int in template_id:
            try:
                template_id_as_int = int(template_id_as_int)
                find_urls_using_template(template_id_as_int, limit)
            except ValueError:
                print 'Skipped %s. Not a valid ID!' % template_id_as_int

def report():
    for id, article_data in articles:
        print 'Template: %d' % id 
        for article in article_data:
            print '\t%s\n\tURL name: %s' % (article.title, article.url_name)
            print