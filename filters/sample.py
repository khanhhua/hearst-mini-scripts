'''
Created on May 14, 2013

@author: khanhhua
'''
from core import sift
@sift('sample')
def sample_filter(url):
    '''
    A filter must determine whether or not the crawler should navigate to that URL or not
    '''
    print 'Sample filter does not filter at all'
    return True or False