'''
Created on Jul 24, 2013

@author: khanhhua
'''
from os import path
from pickle import dump, load
import core
import time

class TaskStore():
    def __init__(self, data_filename = None):
        self.data_filename = 'taskstore-%d.data' % time.time()
        self.tasks = {}
        '''
        @var configs: Multiple configurations keyed by task name (likely to be utility name)
        '''
        self.configs = {}
        
        
        if data_filename != None:
            self.data_filename = data_filename
        if path.exists(self.data_filename):
            print 'Checkpoint data found.'
            with open(self.data_filename,'rb') as f:
                data = load(f)
                if core.configurations['reset'] == False:
                    self.tasks = data.tasks
                else:
                    print 'Use -r (--reset) option to skip restoration'
                self.configs = data.configs
        else:
            print '%s has been created as datastore' % self.data_filename
    
    def dualhash(self, task_args, task_kwargs): 
        args_hash = hash(frozenset(task_args))
        kwargs_hash = hash(frozenset(task_kwargs))
        return "%s.%s" % (args_hash, kwargs_hash)
    
    def checkpoint(self, task_name, task_args, task_kwargs):
        '''
        A task unit
        @param task_name: Make sure this name is UNIQUE. How? I am not sure!
        @type task_name: string 
        '''
        self.tasks[task_name] = self.dualhash(task_args, task_kwargs) 
        with open(self.data_filename,'wb') as f:
            dump(self, f)
        
        print 'An error occurred. Checkpoint %s has been created for task %s' % (self.data_filename, task_name)
    
    def checkstat(self, task_name, *args, **kwargs):
        '''
        @return: 0 if such task was not interrupted
        2 if task was interrupted but arguments not match the last run
        1 if task is to be resumed from here
        @rtype: int        
        '''
        if not task_name in self.tasks:
            return 0
        elif self.tasks[task_name] == self.dualhash(args, kwargs):
            return 1
        else:
            return 2
    
    def record(self, task_name):
        self.configs[task_name] = core.configurations
        # Remove 'reset' option so that such is not persisted
        if 'reset' in self.configs[task_name]:
            del self.configs[task_name]['reset']
            
        with open(self.data_filename,'wb') as f:
            dump(self, f)
        print 'Run configuration recorded for task %s in file %s' % (task_name, self.data_filename)
    
    def commit(self, task_name):
        if task_name in self.tasks:
            del self.tasks[task_name]
        with open(self.data_filename,'wb') as f:
            dump(self, f)
            print 'Run configuration recorded for task %s in file %s' % (task_name, self.data_filename)
    
    def __getstate__(self):
        odict = self.__dict__.copy() # copy the dict since we change it
        del odict['data_filename'] 
        return odict
    
    def __setstate__(self, dicto):
        self.__dict__.update(dicto)   # update attributes
        