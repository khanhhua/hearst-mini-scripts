'''
Created on May 9, 2013

@author: khanhhua
'''
import urllib2, re, os
from core import validators, configurations, log
from arsenal import config, resumable, task_unit
from cookielib import CookieJar
import base64

validation_results = {}

@config(optional=['timeout','recursive', 'depth', 'user', 'pass', 'url', 'list'])
@resumable('validator')
def run():    
    url = configurations['url']
    config_keys = set(configurations.keys())
    if not url == None and set(['user', 'pass']).issubset(config_keys):
        match = re.search('^http://[\w\.\-_]+(:\d+)?', url)
        if match != None:
            home_url = match.group(0) 
            init_urllib2(home_url, configurations['user'], configurations['pass'])
    
    timeout = int(configurations['timeout']) if not configurations['timeout'] == None else 30
        
    if 'list' in configurations and configurations['list'] != None:
        list_file = configurations['list']
        if not os.path.exists(list_file):
            print 'List file does not exist or is not accessible'
            return
        with open(list_file) as f:
            for line in f:
                # each line is a URL
                line = line.strip()
                validate_multiple(line)
    else:
        print 'Accessing %s' % url
        try:
            # basic_auth = base64.b64encode('basic %s:%s' % (configurations['user'], configurations['pass'])) 
            # req = urllib2.Request(url, None, headers={'WWW-Authenticate':basic_auth})
            req = urllib2.urlopen(url)
            content = req.read()
            validate_chain(url, content)
        except urllib2.HTTPError, e:
            print 'HTTP Error. Skipped'
    print 'Validator run completed'

@task_unit('validator')
def validate_multiple(line):
    print 'Accessing %s' % line
    try:
        req = urllib2.urlopen(line)
        content = req.read()
        validate_chain(line, content)
        print ''
    except urllib2.HTTPError, e:
        print 'HTTP Error. Skipped'

def validate_chain(link, content):
    validation_results[link] = []
    log.result(link)
    for validator in validators:        
        if validator.run(content):
            status = (validator.name,'OK')
            validation_results[link].append(status)
        else:
            status = (validator.name,validator.error_msg)
            validation_results[link].append(status)
        log.result("%s: %s" % status)
    
def init_urllib2(home_url, user, pwd):
    mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
    mgr.add_password(None,home_url,user,pwd)
    cj = CookieJar()
    urllib2.install_opener(urllib2.build_opener(urllib2.HTTPCookieProcessor(cj), urllib2.HTTPBasicAuthHandler(mgr)))
    
def report():
#     for link, results in validation_results.items():
#         print link
#         for status in results:
#             # Validator name: Vadiator status
#             print "%s: %s" % status
#         print
    pass