'''
Created on Jul 31, 2013

@author: khanhhua
'''
from core import configurations
from arsenal import config, http_auth
import urllib2
from urlparse import urlparse
import socket


@config(required=['home_url','username','password','datafile'])
def run():
    home_url = configurations['home_url']
    o = urlparse(home_url) 
    
    try:
        with open(configurations['datafile']) as f:
            data = f.read()                        
            if (data):            
                sock = socket.socket()
                sock.connect((o.hostname, int(o.port)))
                sock.send(data)
                
                sock.close()                
    except socket.error, e:
        print e
   