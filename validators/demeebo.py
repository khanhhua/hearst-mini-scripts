'''
Created on May 20, 2013

@author: khanhhua
'''
from core import validator

@validator('meebo-free', 'HMTL contains MEEBO script')
def meebo_free(content): 
    if content.count('window.Meebo') > 0 or content.count("Meebo('domReady')") > 0:
        return False
    return True