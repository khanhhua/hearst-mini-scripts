'''
Created on May 20, 2013

@author: khanhhua
'''
from core import validator

@validator('friend', 'HMTL does not have a "friend"')
def friend(content): 
    if content.count('friend') == 0 and content.count('o b') == 0:
        return False
    return True