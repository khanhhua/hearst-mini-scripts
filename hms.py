'''
Created on May 7, 2013

@author: khanhhua


'''
import sys, re, argparse
import core

def banner():
    '''
    Prints out a welcome banner
    '''
    print '''==============================
WELCOME TO HEARST MINI SCRIPTS
==============================
An interactive console to automate the dull lame job 
of pervasive script updates and massive UT 
    ''';

def usage():
    '''
    Prints usage instructions
    '''
    print '''
NAME
        hms - Hearst (Interactive) Mini Scripts

SYNOPSIS
        hms [options]

DESCRIPTION
        options:
        -i    --interactive            Launch script in interactive mode
        -m    --validators=validator-name    Load and chain the validators
        -u    --utility=utility-name   Load the utility. Built-in utilities include validator, site-crawler, tmpl-mapper
        
        interactive commands:
        validators                        Load and chain the validation modules
        utility                        Load the utility
        run                            Run the loaded utility
    '''
# Config section
utility = None

def command(line):
    pass
    
def init():
    parser = argparse.ArgumentParser(description='hms - Hearst (Interactive) Mini Scripts')
    parser.add_argument('-t','--lastconfig', nargs='?', required=False, default=None, dest='lastconfig')
    parser.add_argument('-u','--utility', nargs=1, required=True, dest='utility')
    parser.add_argument('-v','--validators', nargs='*', dest='validators')
    parser.add_argument('-f','--filters', nargs='*', dest='filters')
    parser.add_argument('-i','--interactive', action='store_const', const=True, dest='interactive')
    parser.add_argument('-x','--extras', nargs='+', action='append', dest='extras')
    parser.add_argument('-r','--reset', nargs='?', required=False, const=True, default=False, dest='reset')
    args = parser.parse_args()
    
    if args.reset != None:
        core.configurations['reset'] = args.reset
    if args.lastconfig != None:
        from core import resume
        from os import path
        from pickle import load
        if path.exists(args.lastconfig):
            with open(args.lastconfig,'rb') as f:
                core.taskstore = load(f)
                core.taskstore.data_filename = args.lastconfig
                task_name = args.utility[0]
                if not task_name in core.taskstore.configs:
                    print 'Could not restore. Last run configuration for "%s" not found.' % task_name
                    return False
                else:
                    core.configurations = core.taskstore.configs[task_name]
        else:
            print 'Could not restore. Task storage %s not found.' % args.lastconfig
            return False
    else:
        if args.interactive:
            core.configurations['interactive'] = True
        if args.validators != None:
            core.configurations['validators'] = args.validators
        if args.filters != None:
            core.configurations['filters'] = args.filters        
        
        core.configurations['utility'] = args.utility[0]

        if args.extras != None:
            extra_options = args.extras[0]
            rgx_option = re.compile('(?P<name>[\w\-]+=)(?P<value>[\-\w,:/\.]*)')
            
            for option in extra_options:
                match = rgx_option.match(option)
                if match != None: 
                    core.configurations[match.group('name')[:-1]] = match.group('value')
    
    if 'utility' in core.configurations:
        load_utility(core.configurations['utility'])
    
    if 'validators' in core.configurations and len(core.configurations['validators']) > 0:
        load_modules(core.configurations['validators'], 'validators')
        
    if 'filters' in core.configurations and len(core.configurations['filters']) > 0:
        load_modules(core.configurations['filters'], 'filters')    
        
    return True

def load_modules(module_names, group):
    for name in module_names:
        name = name.replace('-','_')
        __import__(group + '.' + name)

def load_utility(utility_name):
    global utility
    utility_name = utility_name.replace('-','_')
    m = __import__('utils.' + utility_name)
    utility = getattr(m, utility_name)

def run():
    global utility
    try:
        utility.run()
    except Exception, e:
        from core.log import result_filename
        print 'Activity has been logged in %s' % result_filename
        raise e

def report():
    from core.log import result_filename
    print 'Activity has been logged in %s' % result_filename
    global utility
    try:
        utility.report()
    except:
        print 'Cannot report'

# Main routine
if __name__ == '__main__':
    banner()
    
if not init():
    usage()
    exit(0)
    
run()
report()