'''
Parse the TMPL construct used in Hearst Template Engine

@author: Khanh Hua
'''
import re
import os

EXPRESSIONS = {
   'var': re.compile('<TMPL_VAR\s+(\w+)>', re.IGNORECASE | re.DOTALL),
   'include': re.compile('(\<TMPL_INCLUDE\s+(?:name=")?([_\.\w\-\d\/]+?\.tmpl)"?\>)', re.IGNORECASE | re.DOTALL),
}

class Template(object):
    
    def __init__(self, id, name, file_path, site = 'crosssite', top_level=False):
        self._site = site
        
        self.code = ''
        self.nodes = {}
        self.tmpl_vars = {}
        
        self.deferred_includes = []
        # Maps the original phrase used in code to the referenced template by name
        self.include_to_name_mapping = {}
        
        self._id = id
        self._name = name
        self._file_path = file_path
        self._top_level = top_level
    
    def hasIncludes(self):
        return len(self.deferred_includes) > 0
    
    def hasIncluded(self, other_tmpl): 
        """
        @param other_tmpl: The template we need to check for inclusion
        @type other_tmpl: core.tmpl.Template 
        """
        if not self.hasIncludes(): return False
        
        if other_tmpl._name in self.deferred_includes:
            return True
        else:
            return False
    
    def shortpath(self):
        return self._file_path[len(self._site.base_dir) + 1:]
    
    def include(self, node):
#         node._site = self._site
#         if node._site in node._file_path: 
#             key = node._file_path[node._file_path.rfind(self._site) + len(self._site) + 1:]
#         elif 'crosssite' in node._file_path:
#             key = node._file_path[node._file_path.rfind('crosssite') + len('crosssite') + 1:]
#         else:
#             return
        self.nodes[node._name] = node
    
    def preview(self): pass
    
    def build(self, code):
        self.code = code
        matches = EXPRESSIONS['include'].findall(code)
        if matches != None:
            from os.path import dirname
            site_basedur_len = len(self._site.path())
            
            for match in matches:
                tmpl_filename = match[1]
                if tmpl_filename[:2] == 'xs':
                    node_name = tmpl_filename[:-5]
                    self.deferred_includes.append(node_name)
                    self.include_to_name_mapping[match[1]] = node_name
                    continue
                # 1. Start from the base_template path if one is given
                # 2. Keep looking up the path
                cur_path = dirname(self._file_path)
                tmpl_path = "%s/%s" % (cur_path, tmpl_filename)
                while tmpl_path.startswith(self._site.path()) and not os.path.exists(tmpl_path):
                    # UP WE SEARCH until we hit the site base dir
                    cur_path = dirname(cur_path)
                    tmpl_path = "%s/%s" % (cur_path, tmpl_filename)
                    # print 'Searching: %s' % tmpl_path
                else:
                    if not tmpl_path.startswith(self._site.path()):
                        node_name = tmpl_filename[:-5]
                        self.deferred_includes.append(node_name)
                        self.include_to_name_mapping[match[1]] = node_name
                        # print 'Assuming crossite %s' % node_name
                        continue 
                    
                node_name = tmpl_path[site_basedur_len + 1: -5] # The fully-qualified name
                self.deferred_includes.append(node_name)
                self.include_to_name_mapping[match[1]] = node_name
    
    def tree_view(self, lines=None, depth = 0):
        if lines == None:
            lines = []
#         if '/' in self._name:
#             lines.append('\t'*depth + self._name[self._name.rfind('/')+1:])
#         else:
        lines.append('%s\t%s' % (('    '*depth + self._name).ljust(60), self.shortpath()))
            
        for node_name in self.deferred_includes:
            # node_name = node_name[:-5] if node_name[-5:] == '.tmpl' else node_name  
            if node_name in self.nodes: 
                self.nodes[node_name].tree_view(lines, depth + 1)
        
        if depth == 0:
            return '\n'.join(lines)
    
    def expanded_view(self):
        code = self.code
        matches = EXPRESSIONS['include'].findall(code)
        if not matches == None and len(matches) > 0:
            for match in matches:
                node_name = self.include_to_name_mapping[match[1]] # if match[1][-5:] == '.tmpl' else match[1]  
                node = self.nodes.get(node_name, None)
                if not node == None:
                    # print '### ACCESSING: %s (at: %s) ###' % (node._name, node._file_path)
                    # Annotate expanded template file with [HMS: included template file name] CODE [/HMS: included template file name]
                    expanded = "[HMS: %s]\n%s\n[/HMS: %s]\n" % (match[1], node.expanded_view(), match[1])
                    code = code.replace(match[0], expanded)
                else:
                    code = code.replace(match[0], '%s is missing' % match[1])
        return code

class Site:
    def __init__(self, name, base_dir, configObj = None):
        """
        @param name: Name of the site, name can be crosssite too! We treat crosssite as a site
        @param base_dir: Always points to the /path/to/templates directory 
        """
        self.name = name
        self.base_dir = base_dir
        self.config(configObj)
    
    def config(self, configObj = None):
        """
        Look for site.config file
        """
        if configObj == None:
            with open(os.path.dirname(__file__) + '/site.config') as f:
                s = f.read()
                import json
                decoder = json.JSONDecoder()
                configObj = decoder.decode(s)[self.name]
                self.dir = configObj['dir']
                self.crosssite = configObj['crosssite'] if 'crosssite' in configObj else False
        else:
            self.dir = configObj['dir']
            self.crosssite = configObj['crosssite'] if 'crosssite' in configObj else False
            
    def path(self):
        return "%s/%s" % (self.base_dir, self.dir)
    
    def tmpl(self, tmpl_name, base_template = None):
        """
        @param base_template: The template which includes tmpl_name
        @type base_template: core.tmpl.Template
        @return: Template
        @rtype: core.tmpl.Template
        """
        from os.path import dirname
        
        tmpl_filename = tmpl_name if tmpl_name[-5:] == '.tmpl' else tmpl_name + '.tmpl'  
        
        if not base_template == None:
            # 1. Start from the base_template path if one is given
            # 2. Keep looking up the path
            cur_path = dirname(base_template._file_path)
            tmpl_path = "%s/%s" % (cur_path, tmpl_filename)
            while tmpl_path.startswith(self.path()) and not os.path.exists(tmpl_path):
                # UP WE SEARCH
                cur_path = dirname(cur_path)
                tmpl_path = "%s/%s" % (cur_path, tmpl_filename)
                # print 'SITE %s: %s' % (self.name, tmpl_path)     
        else:
            tmpl_path = "%s/%s" % (self.path(), tmpl_filename)
            if not os.path.exists(tmpl_path):
                return
                
        with open(tmpl_path) as f:
            tmpl = f.read()
        id = 0
        # The name should NOT ever contain the .tmpl
        name = tmpl_name[:-5] if tmpl_name[-5:] == '.tmpl' else tmpl_name
        long_name = ''
        top_level = False
        
        meta_path = tmpl_path.replace('.tmpl', '.meta')
        if os.path.exists(meta_path):
            with open(meta_path) as f:
                for line in f:
                    if not ':' in line: continue
                    (key, val) = line.split(':', 1)
                    val = val.strip()
                    if key == 'id':
                        id = int(val)
                    elif key == 'filename':
                        name = val
                    elif key == 'long_name':
                        long_name = val
                    elif key == 'top_level':
                        top_level = bool(val)
        tmplObj = Template(id, name, tmpl_path, self, top_level)
        tmplObj.build(tmpl)
        
        return tmplObj
    
    def list_files(self):
        from os import walk
        result = []
        #site_dir_len = len(self.path())
        for root, dirs, files in os.walk(self.path()):
            #root = root[-site_dir_len:] # remove the path of the site
            for f in files:
                if f.endswith('.tmpl'):
                    result.append('%s/%s' % (root, f))   
                
        return result

class CrossSite (Site):
    def config(self, configObj = None):
        """
        Look for site.config file
        """
        with open(os.path.dirname(__file__) + '/site.config') as f:
            s = f.read()
            import json
            decoder = json.JSONDecoder()
            configObj = decoder.decode(s)
            
            self.dir = configObj[self.name]['dir']
            self.crosssite = True
            del configObj[self.name]
            
            self._related_sites = {} 
            for siteName, configObj in configObj.items():
                site = Site(siteName, self.base_dir, configObj)
                self._related_sites[siteName] = site
                
    def list_files(self):
        from os import walk
        result = []
        
        for root, dirs, files in os.walk(self.path()):
            for f in files:
                if f.endswith('.tmpl'):
                    result.append('%s/%s' % (root, f))   
        
        for site_name, site in self._related_sites.items():
            files_in_site = site.list_files()
            result.extend(files_in_site)
            
        return result